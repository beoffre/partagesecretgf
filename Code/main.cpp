#include "GF256.h"

#ifndef POLYNOME_H
#define POLYNOME_H

int main(){
    
    Polynome test = *new Polynome();
    //test.printPoly();

    Polynome testRedAvant = *new Polynome(283);
    //testRedAvant.printPoly();
    GF256 gf = *new GF256(2,8,*new Polynome(283));

    Polynome testRed = *new Polynome(283);
    //testRed.printPoly();
    unsigned char sum = gf.somme((unsigned char)255, (unsigned char) 45);


    int a[7] = {1,0,1,1,0,1,1};
    int b[6] = {0,0,0,1,1,1};

    Polynome A = Polynome(6,a);
    A.printPoly();
    Polynome B = Polynome(5,b);
    B.printPoly();
    
    //A.multPoly(B).printPoly();

    Polynome prod = gf.multPoly(A, B);
    prod.printPoly();
    Polynome reduced = gf.reduction(prod.calcValue());
    reduced.printPoly();

    // printf("sum : %d\n",sum);
    // Polynome(255).printPoly();
    // Polynome(45).printPoly();
    // Polynome(sum).printPoly();

    // unsigned char prod = gf.mult(45,21);
    // Polynome(45).printPoly();
    // Polynome(21).printPoly();
    // Polynome(prod).printPoly();

    // prod = gf.mult(83, 202);
    // printf("prod : %d\n",prod);
    // Polynome(83).printPoly();
    // Polynome(202).printPoly();
    // Polynome(prod).printPoly();

    // Polynome(1).printPoly();
    // Polynome(0).printPoly();
    // printf("div %d\n",gf.div(127,21));
}

#endif