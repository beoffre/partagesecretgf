#include <math.h>
#include <stdio.h>

class Polynome{
    public:
    int deg;
    int* coefs;
    //int value;
    Polynome(){
        deg=0;
        coefs=new int[1];
        coefs[0]=0;
        //value = 0;
    }
    Polynome(int d, int* c){
        //printf("cc voici le dernier coef : %d \n",c[d]);
        while(d>0){
            if (c[d]==0){
                d--;
            }
            else{break;}
        }
    
        coefs = new int[d+1];
        for (int i =0;i<d+1;i++){
            coefs[i]=c[i];
        }
        deg=d;
        
        //value = 0;
    }
    Polynome(int* c){
        int d = (sizeof(c)/sizeof(c[0])) - 1;
        while(d>0){
            if (c[d]==0){
                d--;
            }
            else{break;}
        }
    
        coefs = new int[d+1];
        for (int i =0;i<d+1;i++){
            coefs[i]=c[i];
        }
        deg=d;
    }
    Polynome(int val){
        // if (val == 0){int* tab = new int[1];tab[0]=0;Polynome(0,tab);}
        // if (val == 1){int* tab = new int[1];tab[0]=1;Polynome(0,tab);}
        int count=0;
        if (val == 0){deg=0;}
        else{deg = (int)floor(log2(val));}
        //printf("%d, %f\n",deg, log2(val));
        coefs = new int[deg+1];
        while (val){
            if (val%2==1){
                coefs[count]=1;
            }
            else{coefs[count]=0;}
            count++;
            val=val>>1;
        }
        //printf("value = %d \n", this->calcValue());
        //value = this->calcValue();
    }

    int calcValue(){
        int res = 0;
        for (int i =0;i<deg+1;i++){
            res +=coefs[i]*pow(2,i);
        }
        return res;
    }

    void printPoly(){
        printf("degre : %d, val : %d, liste coefs : ",deg, this->calcValue());
        for (int i =0;i<deg+1;i++){
            printf("%d ", coefs[i]);
        }
        printf("\n");
    }

    Polynome multPoly( Polynome Q){

        int * tabRes = new int[deg+Q.deg+1];
        for(int i=0;i<deg+Q.deg+1;i++){tabRes[i]=0;}

        for (int dP=(deg);dP>=0;dP--){
            for (int dQ=(Q.deg);dQ>=0;dQ--){
                tabRes[dP+dQ]+=coefs[dP]*Q.coefs[dQ];
            }
        }
        Polynome res = Polynome(deg+Q.deg,tabRes);
        //res.printPoly();
        return res;
    }
    
    
};