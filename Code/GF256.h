#include "Polynome.h"

class GF256{
    public:

    int prime;
    int n;
    Polynome polyReducteur;
    unsigned char * inverseTable;

    

    GF256(int p, int exp, Polynome poly){
        prime=p;
        n=exp;
        polyReducteur=poly;
        inverseTable=new unsigned char[256];
        inverseTable[0]=0;
        for (int i =1;i<256;i++){
            for(int j=1;j<256;j++){
                if (this->mult(i,j)==1){inverseTable[i]=j;}
            }
        }
    }

    unsigned char somme(unsigned char a, unsigned char b){
        return (a^b);
    }

    unsigned char mult(unsigned char a, unsigned char b){
        unsigned char p = 0;
        while(a&&b){
            if (b&1){p^=a;}
            if (a&0x80){a=(a<<1)^polyReducteur.calcValue();}
            //if (a>=128){a=(a<<1)^0x11b;}
            else{a<<=1;}
            b>>=1;
        }
        return p;
    }

    unsigned char inv(unsigned char a){
        return inverseTable[a];
    }

    unsigned char div(unsigned char a, unsigned char b){
        return this->mult(a,inv(b));
    }


    unsigned char reduction(int a){
        Polynome A = Polynome(a);
        Polynome red = Polynome(polyReducteur.calcValue());
        while(red.deg<A.deg){
            red=Polynome(red.calcValue()<<1);
        }
        int newVal = A.calcValue()^red.calcValue();
        if (newVal>255){
            return reduction(newVal);
        }

        return newVal;
    }


    Polynome lagrangeInterpolation(int** couplesVal, int degree){       // liste de couples (xi,yi) ; degree le deg du poly
        
        for (int i =0;i<degree+1;i++){
            
        }

    }

Polynome multPoly( Polynome P, Polynome Q){

        int * tabRes = new int[P.deg+Q.deg+1];
        for(int i=0;i<P.deg+Q.deg+1;i++){tabRes[i]=0;}

        for (int dP=(P.deg);dP>=0;dP--){
            for (int dQ=(Q.deg);dQ>=0;dQ--){
                tabRes[dP+dQ]=this->somme(tabRes[dP+dQ],this->mult(P.coefs[dP],Q.coefs[dQ]));
            }
        }
        Polynome res = Polynome(P.deg+Q.deg,tabRes);
        //printf("si je mult pour reduire ? %d \n",this->mult(res.calcValue(),1));
        //Polynome(this->mult(res.calcValue(),1)).printPoly();
        //Polynome(this->reduction(res.calcValue())).printPoly();
        //res.printPoly();
        return res;
    }


};